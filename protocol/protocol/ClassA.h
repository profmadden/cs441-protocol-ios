//
//  ClassA.h
//  protocol
//
//  Created by Patrick Madden on 2/4/16.
//  Copyright © 2016 SUNY Binghamton CSD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ExampleProtocol.h"

@interface ClassA : UIView
@property (nonatomic, assign) id<ExampleProtocol>delegate;

@end
