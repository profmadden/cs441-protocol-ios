//
//  ExampleProtocol.h
//  protocol
//
//  Created by Patrick Madden on 2/4/16.
//  Copyright © 2016 SUNY Binghamton CSD. All rights reserved.
//

#ifndef ExampleProtocol_h
#define ExampleProtocol_h
#import <Foundation/Foundation.h>

@protocol ExampleProtocol <NSObject>
- (void)exampleMethod:(float)value;
@end

#endif /* ExampleProtocol_h */
