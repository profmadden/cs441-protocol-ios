//
//  ViewController.h
//  protocol
//
//  Created by Patrick Madden on 2/4/16.
//  Copyright © 2016 SUNY Binghamton CSD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClassA.h"
#import "ClassB.h"

@interface ViewController : UIViewController
@property (nonatomic, strong) IBOutlet ClassA *classA;
@property (nonatomic, strong) IBOutlet ClassB *classB;
@end

