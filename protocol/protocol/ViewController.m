//
//  ViewController.m
//  protocol
//
//  Created by Patrick Madden on 2/4/16.
//  Copyright © 2016 SUNY Binghamton CSD. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize classA, classB;

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
	
	[classA setDelegate:classB];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end
