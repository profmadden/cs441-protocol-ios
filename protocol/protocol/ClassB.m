//
//  ClassB.m
//  protocol
//
//  Created by Patrick Madden on 2/4/16.
//  Copyright © 2016 SUNY Binghamton CSD. All rights reserved.
//

#import "ClassB.h"

@implementation ClassB

-(void)exampleMethod:(float)value
{
	if (color)
	{
		[self setBackgroundColor:[UIColor redColor]];
	}
	else
	{
		[self setBackgroundColor:[UIColor blueColor]];
	}
	color = !color;
}
@end
