//
//  ClassA.m
//  protocol
//
//  Created by Patrick Madden on 2/4/16.
//  Copyright © 2016 SUNY Binghamton CSD. All rights reserved.
//

#import "ClassA.h"

@implementation ClassA
@synthesize delegate;

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
	[delegate exampleMethod:0];
}

@end
